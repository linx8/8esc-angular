class ConnectController {
  constructor($http, $log, $window) {
    this.mailMsg = "";
    this.error = false;
    this.mailSent = false;
    $window.scrollTo(0, 0);

    this.sendMail = () => {
      if (this.name && this.email && this.message) {
        $http.post('scripts/mail.php', {
          name: this.name,
          email: this.email,
          message: this.message
        })
        .success(message => {
          $log.log(message);
          this.clearFields();
          this.error = false;
          this.mailSent = true;
          this.mailMsg = "Thank you for your email, I will respond to you shortly.";
        });
      } else {
        $log.log(`${this.name} ${this.email} ${this.message}`);
        this.mailMsg = "Incomplete fields";
        this.error = true;
      }
    };

    this.clearFields = () => {
      this.email = this.name = this.message = "";
    };
  }
}

export default {
  template: require('./connect.html'),
  controller: ConnectController
};
