import angular from 'angular';

import home from './home';
import connectForm from '../connect/connectForm';

export const homeModule = 'home';
import 'angular-media-queries/match-media';

angular
  .module(homeModule, ['matchMedia'])
  .component('home', home)
  .component('connectForm', connectForm);
