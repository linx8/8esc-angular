class HomeController {
  /** @ngInject */
  constructor($sce, $timeout, screenSize) {
    this.flipIntroCard = false;
    this.flipAll = false;
    this.ethosCards = [
      {
        cardTitle: 'Connect',
        cardDescription: $sce.trustAsHtml('Make an honest connection with <i>local</i> people. Working with businesses - not for them.')
      },
      {
        cardTitle: 'Ideas',
        cardDescription: $sce.trustAsHtml('To facilitate and create technical solutions for great ideas - bringing ideas that the world needs to life.')
      },
      {
        cardTitle: 'Community',
        cardDescription: $sce.trustAsHtml('Work with the community on social initiatives, charities and not-for-profit projects to tackle social, economic and cultural issues.')
      },
      {
        cardTitle: 'Positive Impact',
        cardDescription: $sce.trustAsHtml('I invoice in AU dollars, but I work for positive change and social improvement - aiming to create a more sustainable future.')
      }
    ];

    this.clientListRows = [
      {
        classes: 'circles',
        logos:
        [
          {
            name: 'Skylark Sports',
            src: '/imgs/portfolio/skylark.png',
            link: 'http://skylarksports.com.au',
            class: 'invert'
          },
          {
            name: 'The Barefoot Osteopath',
            src: '/imgs/portfolio/thebarefootosteopath.png',
            link: 'http://thebarefootosteopath.com.au'
          },
          {
            name: 'Serenity Eco Guesthouse',
            src: '/imgs/portfolio/serenityecoguesthouse.png',
            link: 'http://serenityecoguesthouse.com'
          },
          {
            name: 'Addicted 2 Dance Studios',
            src: '/imgs/portfolio/a2d-circle-logo.png',
            link: 'http://a2dstudios.com.au'
          }
        ]
      },
      {
        classes: '',
        logos:
        [
          {
            name: 'Apexx Wealth',
            src: '/imgs/portfolio/apexx.png',
            link: 'http://apexxwealth.com.au'
          },
          {
            name: 'Australia Vietnam Young Leaders Dialogue',
            src: '/imgs/portfolio/avyld.png',
            link: 'http://serenityecoguesthouse.com'
          },
          {
            name: 'H3 Space',
            src: '/imgs/portfolio/h3space-logo.png',
            link: 'http://h3space.com.au'
          },
          {
            name: 'Steer North',
            src: '/imgs/portfolio/steernorth.png',
            link: 'http://steernorth.org.au'
          }
        ]
      },
      {
        classes: '',
        logos:
        [
          {
            name: 'Papercrane Designs',
            src: '/imgs/portfolio/papercranedesigns.png',
            link: 'http://papercranedesigns.com'
          },
          {
            name: 'Saibu No Akuma',
            src: '/imgs/portfolio/saibu.png',
            link: 'http://saibunoakuma.com.au'
          },
          {
            name: 'Touch of Siam',
            src: '/imgs/portfolio/touchofsiam.png',
            link: 'http://touchofsiamkingston.com.au/'
          },
          {
            name: 'Yogathon for Peace',
            src: '/imgs/portfolio/yogathonforpeace.png',
            link: 'https://www.facebook.com/International-Yogathon-For-World-Peace-1029826993740784'
          }
        ]
      }
    ];
    this.$onInit = () => {
      $timeout(() => {
        this.flipIntroCard = true;
      }, 1000);
    };

    this.ethosFlipAll = () => {
      this.flipAll = !this.flipAll;
    };

    if (screenSize.is('xs') || screenSize.is('sm')) {
      this.flipAll = true;
    }
  }
}

export default {
  template: require('./home.html'),
  controller: HomeController
};
