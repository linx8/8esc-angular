const PortfolioScrollLockDirective = () => {
  return {
    bindings: {},
    controller: class ScrollLockController {
      constructor($scope, $window, $log) {
        $scope.window = $window;
        $scope.log = $log.log;
        $scope.log('test');
      }
    },
    compile: ($element, $attrs) => {
      return {
        post: ($scope, $element) => {
          angular.element($scope.window).bind('scroll', () => {
            if ($scope.window.scrollY >= 150) {
              $scope.log('Boom!');
            }
          });
        }
      };
    }
  };
};

export default PortfolioScrollLockDirective;
