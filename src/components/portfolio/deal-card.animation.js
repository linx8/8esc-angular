const DealCardAnimation = () => {
  return {
    enter: element => {
      const e = global.$(element);
      e.addClass('card-spin');
      e.animate({left: `${e.attr('index') * 150 + 200}px`, opacity: '1'}, 500, () => {
        e.addClass('card-tilt');
      });
    },
    leave: element => {
      const e = global.$(element);
      e.stop().animate({left: '0px'}, 300, () => {
        e.stop().animate({opacity: '0'}, 200);
      });
    }
  };
};

export default DealCardAnimation;
