class PortfolioController {
  /** @ngInject */
  constructor(screenSize, $location) {
    this.selectedClient = undefined;
    if (screenSize.is('xs')) {
      $location.url('/simple-portfolio');
    }
  }
}

export default {
  template: require('./portfolio.html'),
  controller: PortfolioController
};
