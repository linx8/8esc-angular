import angular from 'angular';

import portfolio from './portfolio';
import simplePortfolio from './simple-portfolio';
import PortfolioDeckDirective from './portfolio-deck.directive';
import DealCardAnimation from './deal-card.animation';

export const portfolioModule = 'portfolio';

import 'angular-media-queries/match-media';

angular
  .module(portfolioModule, ['matchMedia'])
  .component('portfolio', portfolio)
  .component('simplePortfolio', simplePortfolio)
  .component('portfolioDeck', PortfolioDeckDirective)
  .animation('.current-card', DealCardAnimation);

