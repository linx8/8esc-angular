const PortfolioDeckDirective = {
  bindings: {
    selectedClient: '='
  },
  controller: class PortfolioDeckController {
    constructor($log, $timeout, screenSize, $http, $location) {
      this.firstDeal = true;
      this.log = $log.log;
      this.nextCard = 0;
      this.currentHand = [];
      this.mobileMode = false;
      this.dealLock = false;

      if (screenSize.is('xs')) {
        // this.numDeal = 1;
        // no deal! Show all the cards instead or perhaps a slider
        $location.url('/simple-portfolio');
      } else if (screenSize.is('sm')) {
        this.numDeal = 3;
      } else {
        this.numDeal = 5;
      }

      screenSize.on('xs', match => {
        if (match && this.numDeal !== 1) {
          this.numDeal = 1;
          this.log('xs!');
          this.shrinkHand();
        }
      });

      screenSize.on('sm', match => {
        if (match && this.numDeal !== 3) {
          this.numDeal = 3;
          this.log('sm!');
          this.shrinkHand();
        }
      });

      screenSize.on('md, lg, xl', match => {
        if (match && this.numDeal !== 5) {
          this.numDeal = 5;
          this.log('md or lg!');
        }
      });

      this.showClient = client => {
        this.selectedClient = client;
      };

      $http.get('components/portfolio/clients.json')
        .then(response => {
          this.cards = response.data;

          $timeout(() => {
            this.handleCards();
          }, 500);
        });

      /* Deal cards out to current hand */
      this.handleCards = () => {
        if (this.firstDeal) {
          this.firstDeal = false;
          this.dealCards();
        } else if (!this.dealLock) {
          this.dealLock = true;
          $timeout(() => {
            this.dealLock = false;
          }, 2000);
          const promise = this.shuffleHand();
          promise.then(() => {
            $timeout(() => {
              this.dealCards();
            }, 800);
          });
        }
      };

      this.dealCards = () => {
        let toDeal = this.numDeal;
        if (this.nextCard === this.cards.length) {
          // finished a complete deal perfectly
          this.nextCard = 0;
        } else if (this.nextCard + this.numDeal > this.cards.length) {
          // We need to do a short deal
          toDeal = this.cards.length - this.nextCard;
        }

        for (let i = 0; i < toDeal; i++) {
          $timeout(() => {
            if (this.nextCard > this.cards.length - 1) {
              this.nextCard = 0;
            }
            this.currentHand.push(this.cards[this.nextCard++]);
          }, i * 250);
        }
      };

      /* Shuffle current hand back to deck */
      this.shuffleHand = () => {
        const len = this.currentHand.length;
        return $timeout(() => {
          for (let i = 0; i < len; i++) {
            $timeout(() => {
              this.currentHand.splice(0, 1);
            }, 200 + i * 80);
          }
        }, 10);
      };
      this.shrinkHand = () => {
        while (this.currentHand.length > this.numDeal) {
          this.log(`shrinking! ${this.currentHand.length}`);
          this.currentHand.pop();
          this.nextCard--;
        }
      };
    }
  },
  template: require('./portfolio-deck.directive.html')
};

export default PortfolioDeckDirective;
