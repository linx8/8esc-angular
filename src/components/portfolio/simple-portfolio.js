class SimplePortfolioController {
  /** @ngInject */
  constructor($http, $log, $timeout) {
    this.log = $log.log;
    $http.get('components/portfolio/clients.json')
      .then(response => {
        this.log(response);
        this.cards = response.data;
      });
  }
}

export default {
  template: require('./simple-portfolio.html'),
  controller: SimplePortfolioController
};
