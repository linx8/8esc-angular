class AboutController {
  /** @ngInject */
  constructor($window) {
    $window.scrollTo(0, 0);
  }
}

export default {
  template: require('./about.html'),
  controller: AboutController
};
