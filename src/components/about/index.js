import angular from 'angular';
import about from './about';
import AboutDeckDirective from './about-deck.directive';

export const aboutModule = 'about';

angular
  .module(aboutModule, ['ui.bootstrap'])
  .component('about', about)
  .component('aboutDeck', AboutDeckDirective);
