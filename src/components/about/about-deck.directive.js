const AboutDeckDirective = {
  bindings: {},
  controller: class AboutDeckController {
    constructor($log, $interval) {
      this.interval = 3000;
      this.images = ['linx-mug-1.jpg', 'linx-point.jpg', 'bboy.jpg'];
      this.log = $log.log;
    }
  },
  template: require('./about-deck.directive.html')
};

export default AboutDeckDirective;
