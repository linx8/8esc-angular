import angular from 'angular';
import 'angular-ui-router';
import 'angular-ui-bootstrap/dist/ui-bootstrap-tpls';
import 'angular-animate';
import 'angular-touch';

import jQuery from 'jquery/dist/jquery.min';
global.jQuery = global.$ = jQuery;

import routesConfig from './routes';

import header from './components/core/header';
import footer from './components/core/footer';
import {homeModule} from './components/home/index';
import {aboutModule} from './components/about/index';
import {portfolioModule} from './components/portfolio/index';
import 'bootstrap/dist/css/bootstrap.min.css';

import './scss/index.scss';

angular
  .module('app', ['ui.router', 'ui.bootstrap', 'ngAnimate', 'ngTouch', homeModule, aboutModule, portfolioModule])
  .config(routesConfig)
  .component('linxHeader', header)
  .component('linxFooter', footer);
