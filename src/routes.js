export default routesConfig;

/** @ngInject */
function routesConfig($stateProvider, $urlRouterProvider, $locationProvider) {
  $locationProvider.html5Mode(false).hashPrefix('!');
  $urlRouterProvider.otherwise('/');

  $stateProvider
    .state('home', {
      url: '/',
      component: 'home'
    })
    .state('about', {
      url: '/about',
      component: 'about'
    })
   .state('portfolio', {
     url: '/portfolio',
     component: 'portfolio'
   })
   .state('simplePortfolio', {
     url: '/simple-portfolio',
     component: 'simplePortfolio'
   })
   .state('connect', {
     url: '/connect',
     component: 'connectForm'
   });
}
