module.exports = {
  extends: [
    'angular'
  ],
  rules: {
    'angular/no-service-method': 0,
    "no-unused-vars": ["error", { "vars": "all", "args": "none" }],
     "no-console": 0,
     'angular/log': 0,
     'no-loop-func': 0

  }
}
